import { Component, Input } from '@angular/core';
import { FaceSnap } from '../models/face-snap.model';
import { FaceSnapsService } from '../services/face-snaps-service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-face-snap',
  templateUrl: './single-face-snap.component.html',
  styleUrls: ['./single-face-snap.component.scss']
})
export class SingleFaceSnapComponent {
  @Input() faceSnap!: FaceSnap;

  constructor(private faceSnapService: FaceSnapsService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    const id = +this.route.snapshot.params['id'];
    this.faceSnap = this.faceSnapService.getFaceSnapById(id);
  }

  onAddSnap() {
    this.faceSnapService.snapFaceSnapById(this.faceSnap.id, this.faceSnap.isSnapped ? 'unsnap': 'snap');
    this.faceSnap.isSnapped = !this.faceSnap.isSnapped;
  }

}
