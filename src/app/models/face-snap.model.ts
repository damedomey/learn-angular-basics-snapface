export class FaceSnap {
    id!: number;
    title!: string; 
    description!: string; 
    createdAt!: Date; 
    snaps!: number; 
    imageUrl!: string; 
    isSnapped!: boolean; 
    location?: string; 
}
